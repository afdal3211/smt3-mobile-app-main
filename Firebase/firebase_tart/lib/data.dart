import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_tart/post_model.dart';
import 'package:firebase_tart/add.dart';
import 'package:firebase_tart/edit.dart';

Stream<List<Post>> post() =>
  FirebaseFirestore.instance.collection('post').snapshots().map(
    (snapshot) => snapshot.docs.map((e) => Post.fromJson(e.data())).toList());

class Data extends StatefulWidget {
  const Data({ Key? key }) : super(key: key);

  @override
  State<Data> createState() => _DataState();
}

class _DataState extends State<Data> {
  final inputcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),

      body: StreamBuilder<List<Post>>(
        stream: post(),
        builder: (context, snapshot){
          print(snapshot);
          if (snapshot.data == null){
            return const Center(
              child: CircularProgressIndicator()
            );
          }

          if (snapshot.hasData){
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, int index){
                return SingleChildScrollView(
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(snapshot.data![index].body),
                            Row(
                              children: [
                                IconButton(
                                  icon: const Icon(Icons.edit),
                                  onPressed: () {
                                    Navigator.push(
                                      context, MaterialPageRoute(builder: (context) => EditData(id: snapshot.data![index].id))
                                    );
                                  } 
                                  ),

                                  IconButton(
                                    icon: const Icon(Icons.delete),
                                    onPressed: () {
                                      FirebaseFirestore.instance.collection('post').doc(snapshot.data![index].id).delete();
                                    }
                                    )
                              ]
                            )
                          ]
                        ),

                        const Divider(
                          color: Colors.black,
                          height: 20,
                          thickness: 1
                        )

                      ]
                    )
                  )
                );

              }
              );
          }

          else {
            return const Center(
              child: CircularProgressIndicator()
            );
          }
        }
        ),

        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () => {
            Navigator.push(context, MaterialPageRoute(builder: (context) => const AddData()))
          }
          
        )
      
    );
  }
}