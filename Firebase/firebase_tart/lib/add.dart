import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'post_model.dart';

class AddData extends StatefulWidget {
  const AddData({ Key? key }) : super(key: key);

  @override
  State<AddData> createState() => _AddDataState();
}

class _AddDataState extends State<AddData> {
  // ignore: non_constant_identifier_names
  final InputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Post")
      ),

      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            TextField(
              controller: InputController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'placeholder',
              )
            ),

            ElevatedButton(
              onPressed: () async {
                final input = InputController.text;
                final id = 
                  FirebaseFirestore.instance.collection('post').doc().id;

                final data = Post(
                  id: id,
                  body: input,
                ).toJSON();

                await FirebaseFirestore.instance.collection('post').doc(id).set(data);

                Navigator.pop(context);
              },

              child: const Text('Create'))
          ])
      )
      
    );
  }
}