class Post{
  String id;
  String body;

  Post({
    required this.id,
    required this.body,
  });

  Map<String, dynamic> toJSON() => {'id':id, 'body':body};

  factory Post.fromJson(Map<String, dynamic> json){
    return Post(
    id: json['id'], 
    body: json['body']
    );
  }
}