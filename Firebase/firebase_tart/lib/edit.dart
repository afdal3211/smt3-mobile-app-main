
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EditData extends StatefulWidget {
  const EditData({ Key? key, required this.id }) : super(key: key);

  final String id;

  @override
  State<EditData> createState() => _EditDataState();
}

class _EditDataState extends State<EditData> {
  final inputcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Post')
      ),

      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            TextField(
              controller: inputcontroller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'placeholder'
              )
            ),

            ElevatedButton(
              onPressed: () async {
                final input = inputcontroller.text;

                await FirebaseFirestore.instance.collection('post').doc(widget.id).update({'body':input});

                Navigator.pop(context);
              }, 

              child: const Text('Create'))
          ])
      )
      
    );
  }
}