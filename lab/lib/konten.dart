class Konten{
  final String title;
  final String category;
  final String urlImage;

  const Konten(
    {required this.title, required this.category, required this.urlImage}
  );
}

const allKonten = [
  Konten(title: 'Selamat Pagi Dari BSD', category: 'STEMUP', urlImage: 'https://www.prasetiyamulya.ac.id/wp-content/uploads/2021/12/Wisuda-2021-Prof-Djisman-Sekolah-Bisnis-Ekonomi-dan-STEM-Universitas-Prasetiya-Mulya-e1639030455155-400x245.jpg'),
  Konten(title: 'Selamat Siang Dari BSD', category: 'STEMUP', urlImage: 'https://www.prasetiyamulya.ac.id/wp-content/uploads/2020/06/news-banner-4-%E2%80%98Storyteller%E2%80%99-Prasmul-Bersinar-di-AFECA-Asian-MICE-Youth-2019-universitas-prasetiya-mulya-800x351.png'),
  Konten(title: 'Berita STEM Terkini', category: 'STEMUP', urlImage: 'https://www.prasetiyamulya.ac.id/wp-content/uploads/2021/12/Wisuda-2021-Prof-Djisman-Sekolah-Bisnis-Ekonomi-dan-STEM-Universitas-Prasetiya-Mulya-e1639030455155-400x245.jpg'),
];