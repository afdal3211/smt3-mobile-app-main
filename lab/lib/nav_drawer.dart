import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 130,
            child: const DrawerHeader(
              margin: EdgeInsets.all(10),
              child: ListTile(
                contentPadding: EdgeInsets.only(left: 0),
                leading: CircleAvatar(
                  child: Icon(Icons.person, size: 40, color: Colors.white),
                  backgroundColor: Colors.grey,
                  radius: 30,
                ),

                title: Text(
                  'Muhammad Afdhal',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  'Lihat Profile',
                  style: TextStyle(),
                ),
              )
            )
          ),

          

          ListTile(
            title: const Text('Beranda', style: TextStyle(fontWeight: FontWeight.bold)),
            onTap: () {},
          ),
          
          ListTile(
            title: const Text('Riwayat Acara', style: TextStyle(fontWeight: FontWeight.bold)),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Survey', style: TextStyle(fontWeight: FontWeight.bold)),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Pengaturan', style: TextStyle(fontWeight: FontWeight.bold)),
            onTap: () {},
          ),
          ListTile(
            title: const Text('Keluar', style: TextStyle(fontWeight: FontWeight.bold)),
            onTap: () {},
          ),

        ]
      ),
      
    );
  }
}