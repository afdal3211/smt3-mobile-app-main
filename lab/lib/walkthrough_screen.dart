import 'package:flutter/material.dart';
import 'package:lab/login_screen.dart';
import 'package:nb_utils/nb_utils.dart';
// import 'package:dots_indicator/dots_indicator.dart';

import 'package:lab/home_screen.dart';
import 'package:lab/image_list.dart';

class WalkThroughScreen extends StatefulWidget {
  const WalkThroughScreen({ Key? key }) : super(key: key);

  @override
  State<WalkThroughScreen> createState() => _WalkThroughScreenState();
}

class _WalkThroughScreenState extends State<WalkThroughScreen> {
  List<String> imgPage = <String> [
    walkThroughImage1,
    walkThroughImage2,
    walkThroughImage3,
  ];

  int position = 0;

  PageController? pageController;

  @override
  void initState(){
    super.initState();
    init();
  }

  Future<void> init() async{
    pageController = PageController(initialPage: position, viewportFraction: .75);
  }

  @override
  void setState(fn){
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Column(
          children: [
            70.height,
            SizedBox(
              height: context.height()*.5,
              child: PageView.builder(
                controller: pageController,
                scrollDirection: Axis.horizontal,
                itemCount: imgPage.length,
                itemBuilder: (context, index){
                  return AnimatedContainer(
                    duration: 500.milliseconds,
                    height : context.height()*.45,
                    margin: const EdgeInsets.only(left: 16, right: 8),
                    child: Image.asset(
                      imgPage[index], fit: BoxFit.cover, width: context.width(),
                    ).cornerRadiusWithClipRRect(16),
                    );
                },
                onPageChanged: (value) {
                  setState( () { position = value;} );
                }
                )
            ),

            20.height,

            DotIndicator(
              pageController: pageController!,
              pages : imgPage,
              indicatorColor: Colors.red,
              ),

            16.height,

            Text('Selamat Datang di NextGen', style: boldTextStyle(size: 20)).paddingOnly(left: 16, right: 16),

            Text('Temukan info seminar, beasiswa, lomba, magang, dan detail lainnya disini',
              style: secondaryTextStyle(), textAlign: TextAlign.center
            ).paddingOnly(left: 16, right: 16)

          ]
        ),

        Positioned(
          bottom: 16, left: 16, right: 16,

          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppButton(
                width: (context.width()-(3*16))*.5,
                height: 60,
                text: "Lewati",
                textStyle: boldTextStyle(),
                shapeBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                onTap: () {
                  const LoginScreen().launch(context);
                },
                color: Colors.grey,
                hoverColor: Colors.transparent,
                splashColor: Colors.transparent,
                focusColor: Colors.transparent,
              ),

              10.width,

              AppButton(
                width: (context.width()-(3*16))*.5,
                text: position < 2 ? 'Selanjutnya' : 'Selesai',
                height: 60,
                elevation: 0,
                shapeBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                color: Colors.red,
                textStyle: boldTextStyle(color: Colors.white),
                onTap: () {
                  if (position < 2){
                    pageController!.animateToPage(position + 1, duration: const Duration(microseconds: 1000), curve: Curves.easeIn);
                  }
                  else if (position == 2){
                    const LoginScreen().launch(context,isNewTask: true);
                  }
                },
              )
            ]
          )
        )
      ])
    );
  }

  
}