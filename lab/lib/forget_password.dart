import 'package:flutter/material.dart';
import 'package:lab/login_screen.dart';

class ForgetScreen extends StatefulWidget {
  const ForgetScreen({ Key? key }) : super(key: key);

  @override
  State<ForgetScreen> createState() => _ForgetScreenState();
}

class _ForgetScreenState extends State<ForgetScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register Screen'),
        backgroundColor: Colors.red,
        ),

      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: Image.asset('assets/img/NextGen.jpg', fit: BoxFit.cover, height: 90),
            ),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: emailController,
                decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Email Address'),
              ),
            ),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: phoneController,
                decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Phone Number'),
              ),
            ),

            Container(
                height: 60,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: ElevatedButton(
                  child: 
                    const Text('Forget Password'),
                    style: ButtonStyle( backgroundColor: MaterialStateProperty.all(Colors.red)),
                    onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen())) 
                ),
              ),

          ])
          ),
      
    );
  }
}