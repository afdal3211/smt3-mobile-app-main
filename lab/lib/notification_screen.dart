import 'package:flutter/material.dart';
import 'package:lab/home_screen.dart';
import 'package:nb_utils/nb_utils.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notifikasi', style: TextStyle(color: Colors.white)),

        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen())),
        )
      ),

      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 100,
                    color: Colors.grey,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 2),
                      child: Image.asset('assets/img/Banner.jpg',fit: BoxFit.cover),
                    )
                  ).cornerRadiusWithClipRRect(16).expand(flex: 1),

                  10.width,

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const Text('Selamat Pagi dari BSD', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.red),
                      maxLines: 1, overflow: TextOverflow.ellipsis),
                      const Text('Hujan Gak ya...', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, color: Colors.black),
                      maxLines: 2, softWrap: true, overflow: TextOverflow.ellipsis),
                      
                    ]
                  ).expand(flex: 2),
                ]
              ),

              20.height,

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 100,
                    color: Colors.grey,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 2),
                      child: Image.asset('assets/img/Banner.jpg',fit: BoxFit.cover),
                    )
                  ).cornerRadiusWithClipRRect(16).expand(flex: 1),

                  10.width,

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const Text('Selamat Pagi dari BSD', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.red),
                      maxLines: 1, overflow: TextOverflow.ellipsis),
                      const Text('Hujan Gak ya...', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, color: Colors.black),
                      maxLines: 2, softWrap: true, overflow: TextOverflow.ellipsis),
                      
                    ]
                  ).expand(flex: 2),
                ]
              ),
            ]
          )
        )
      )
      
    );
  }
}