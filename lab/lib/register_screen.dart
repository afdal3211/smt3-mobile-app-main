import 'package:flutter/material.dart';
import 'package:lab/login_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({ Key? key }) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register Screen'),
        backgroundColor: Colors.red,
        ),

      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: Image.asset('assets/img/NextGen.jpg', fit: BoxFit.cover, height: 90),
            ),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Name'),
              ),
            ),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: emailController,
                decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Email Address'),
              ),
            ),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Password'),
              ),
            ),

            Container(
              padding: const EdgeInsets.all(10),
              child: TextField(
                controller: phoneController,
                decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Phone Number'),
              ),
            ),

            Container(
              padding: const EdgeInsets.fromLTRB(10,0,10,0),
              child: const Text(
              'Dengan mendaftar, Anda menyetujui syarat dan ketentuan, serta kebijakan privasi kami.',)
            ),

            Container(
                height: 60,
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: ElevatedButton(
                  child: 
                    const Text('Login'),
                    style: ButtonStyle( backgroundColor: MaterialStateProperty.all(Colors.red)),
                    onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen())) 
                ),
              ),

            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('Already Have Account ?'),
                  TextButton(
                    child: const Text(
                      'Sign in',
                      style: TextStyle(color: Colors.red, fontSize: 20)),
                      onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const LoginScreen()))          
                  )
                ],
              ),

          ])
          ),
      
    );
  }
}