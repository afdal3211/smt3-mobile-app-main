import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:lab/home_screen.dart';
import 'package:lab/notificationservices.dart';

import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/cupertino.dart';

import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;

class EventList extends StatefulWidget {
  const EventList({Key? key}) : super (key: key);

  @override
  State<EventList> createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  @override
  void initState(){
    super.initState();
    tz.initializeTimeZones();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('STEMPrenuer Day', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, color: Colors.red),
                  maxLines: 1,overflow: TextOverflow.ellipsis),
                  
                  4.height,

                  const Text('Ayo Ikuti ICASTEM 2021: The 2nd International Conference on Applied Sciences, Technology, Engineering, and Mathematics',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black),
                  maxLines: 2, overflow: TextOverflow.ellipsis,softWrap: true,),

                  10.height,

                  Container(
                    height: 40,
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Register'),
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
                      onPressed: () => {
                        
                        NotificationService().showNotification(
                          1, 
                          "Registratsi Berhasil", 
                          'Berhasil', 
                          3)
                      }
                    ),
                  ).cornerRadiusWithClipRRect(10),
                ],
              ).expand(flex: 2),

              4.width,

              Container(
                height: 100,
                color: Colors.grey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2),
                  child: Image.asset('assets/img/Banner.jpg', fit: BoxFit.cover,),
                  ),
              ).cornerRadiusWithClipRRect(16).expand(flex: 1),

              18.height,
            ]
          ),

          8. height,

          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('STEMPrenuer Day 2', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, color: Colors.red),
                  maxLines: 1,overflow: TextOverflow.ellipsis),
                  
                  4.height,

                  const Text('Ayo Ikuti ICASTEM 2021: The 2nd International Conference on Applied Sciences, Technology, Engineering, and Mathematics',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black),
                  maxLines: 2, overflow: TextOverflow.ellipsis,softWrap: true,),

                  10.height,

                  Container(
                    height: 40,
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Register'),
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
                      onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen()))
                    ),
                  ).cornerRadiusWithClipRRect(10),
                ],
              ).expand(flex: 2),

              4.width,

              Container(
                height: 100,
                color: Colors.grey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2),
                  child: Image.asset('assets/img/Banner.jpg', fit: BoxFit.cover,),
                  ),
              ).cornerRadiusWithClipRRect(16).expand(flex: 1),

              18.height,
            ]
          ),

          8. height,

          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('STEMPrenuer Day 3', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, color: Colors.red),
                  maxLines: 1,overflow: TextOverflow.ellipsis),
                  
                  4.height,

                  const Text('Ayo Ikuti ICASTEM 2021: The 2nd International Conference on Applied Sciences, Technology, Engineering, and Mathematics',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black),
                  maxLines: 2, overflow: TextOverflow.ellipsis,softWrap: true,),

                  10.height,

                  Container(
                    height: 40,
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text('Register'),
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
                      onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen()))
                    ),
                  ).cornerRadiusWithClipRRect(10),
                ],
              ).expand(flex: 2),

              4.width,

              Container(
                height: 100,
                color: Colors.grey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 2),
                  child: Image.asset('assets/img/Banner.jpg', fit: BoxFit.cover,),
                  ),
              ).cornerRadiusWithClipRRect(16).expand(flex: 1),

              18.height,
            ]
          ),
        ],
      ),
    );
  }
}