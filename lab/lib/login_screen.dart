import 'package:flutter/material.dart';
import 'package:lab/register_screen.dart';
import 'package:lab/forget_password.dart';
import 'package:lab/home_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({ Key? key }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    @override
    Widget build(BuildContext context){
      return Scaffold(
        appBar: AppBar(
          title: const Text('Login Screen'),
        ),

        body: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Image.asset(
                      'assets/img/NextGen.jpg',
                      fit: BoxFit.cover,
                      height: 90,
                      ),
                  ],
                ),
              ),

              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller: emailController,
                  decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Email Adress'),
                )
              ),

              Container(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: const InputDecoration(border: OutlineInputBorder(), labelText: 'Password'),
                )
              ),

              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: ElevatedButton(
                  child: 
                    const Text('Login'),
                    style: ButtonStyle( backgroundColor: MaterialStateProperty.all(Colors.red)),
                    onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const HomeScreen())), 
                ),
              ),
              

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text('Does not have account ?'),
                  TextButton(
                    child: const Text(
                      'Sign up',
                      style: TextStyle(color: Colors.red, fontSize: 20)),
                      onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const RegisterScreen())),           
                  )
                ],
              ),

              TextButton(
                child: const Text(
                  'forget Password',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const ForgetScreen())),
              )

              
            ],
          ),
          ),
      );

    }
  
}