import 'package:flutter/material.dart';
import 'package:lab/splash_screen.dart';
import 'package:lab/notificationservices.dart';

void main(){
  WidgetsFlutterBinding.ensureInitialized();
  NotificationService().initNotifications();
  
  runApp(const Lab());
}

class Lab extends StatelessWidget {
  const Lab({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab',
      theme: ThemeData(
        primarySwatch: Colors.red
      ),
      home: const SplashScreen(),
      
    );
  }
}