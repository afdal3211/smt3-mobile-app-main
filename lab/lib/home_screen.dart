import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

import 'package:lab/nav_drawer.dart';
import 'package:lab/artikel_list.dart';
import 'package:lab/event_list.dart';
import 'notification_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin{
  final GlobalKey<ScaffoldState>  _scaffoldKey = new GlobalKey<ScaffoldState>();
  static const List<Tab> myTabs = <Tab>[
    Tab(
      text: 'Semua',
    ),
    Tab(
      text: 'STEMPreneur Day',
    ),
    Tab(
      text: 'STEMPreneur Fair',
    ),
    Tab(
      text: 'STEMPreneur',
    ),
  ];

  late TabController _tabController;

  PageController? pageController;
  int pageIndex = 0;

  @override
  void initState(){
    super.initState();
    _tabController = TabController(length: myTabs.length, vsync: this);
  }
  
  @override
  void dispose(){
    _tabController.dispose();
    super.dispose();
  }

  Future<void> init() async{
    pageController = PageController(initialPage:pageIndex, viewportFraction: 0.9);
  }

  Future<void> _handleRefresh() async {
    return await Future.delayed(Duration(seconds: 2));
  }


  @override
  Widget build(BuildContext context){
    return DefaultTabController(
    length: myTabs.length,
    child: Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          color: Colors.red,
          onPressed: () {_scaffoldKey.currentState!.openDrawer();},
          ),
        title: const Text('NextGen', style: TextStyle(color: Colors.red,fontSize: 20, fontWeight: FontWeight.bold)),

        actions: [
          IconButton(onPressed: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const NotificationScreen())), icon: const Icon(Icons.notifications_none, color: Colors.red,))
        ],

        backgroundColor: Colors.white,
        centerTitle: true,

        bottom: TabBar(
          tabs: myTabs,
          controller: _tabController,
          labelColor: Colors.black,
          unselectedLabelColor: Colors.grey,
          isScrollable: true,
          indicatorColor: Colors.red,
          indicatorWeight: 3,
          indicatorSize: TabBarIndicatorSize.tab,
        ),
      ),

      drawer: NavDrawer(),

      body: LiquidPullToRefresh(
        onRefresh: _handleRefresh,
        child: SingleChildScrollView(child: Container( child:Column(children: [ Column(
        children: [
          SizedBox(
            height: 200,
            child: ListView(
              controller: pageController,
              scrollDirection: Axis.horizontal,
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Image.asset(
                      'assets/img/Banner.jpg',
                      fit: BoxFit.cover,
                      height: 160
                    ).cornerRadiusWithClipRRect(16).paddingRight(5)
                  ],)
                ),

                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Image.asset(
                      'assets/img/Banner.jpg',
                      fit: BoxFit.cover,
                      height: 160
                    ).cornerRadiusWithClipRRect(16).paddingRight(5)
                  ],)
                ),

                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Image.asset(
                      'assets/img/Banner.jpg',
                      fit: BoxFit.cover,
                      height: 160
                    ).cornerRadiusWithClipRRect(16).paddingRight(5)
                  ],)
                ),

                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                  child: Column(children: [
                    Image.asset(
                      'assets/img/Banner.jpg',
                      fit: BoxFit.cover,
                      height: 160
                    ).cornerRadiusWithClipRRect(16).paddingRight(5)
                  ],)
                ),

              ]
            ),
          ).paddingOnly(left: 8, right: 8),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text('Artikel Baru', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              Text('Lihat Semua',style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red))
            ]
            ).paddingOnly(left: 16, right: 16),
          ArtikelList(),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text('Acara Terbaru', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              Text('Lihat Semua',style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red))
            ]
            ).paddingOnly(left: 16, right: 16),
          EventList(),
        ],
        
      )
      
    
      
    ]))))
    ));
  }
}